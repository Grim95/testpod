//
//  KGRect.h
//  TestPod
//
//  Created by Grim on 14/10/15.
//  Copyright (c) 2015 =. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KGRect : NSObject

@property int width;
@property int height;

@end
